#!/usr/bin/env python
# coding: utf-8

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-2
#
# Developed By: Pixelwolf

from fabric.api import env
from fabric.api import lcd
from fabric.api import local
from fabric.api import puts
from fabric.api import run

import datetime
import os
import time
import sys

from config import ConfigFile


BASE_DIR = os.path.dirname(__file__)

env.app_root = BASE_DIR
env.project_settings_file = os.path.join(BASE_DIR, 'project.ini')


def _get_virtualenv_path():
    virtualenv_home = os.getenv('VIRTUALENV_HOME', None)

    if not virtualenv_home:
        return os.path.join(env.app_root, '.venv')

    return os.path.join(virtualenv_home, env.app_name)


def docker_compose(command):
    docker_compose_file = '-f '+'-f '.join(env.docker_compose_file)
    run('cd %s; docker-compose %s %s' % (env.app_root, docker_compose_file, command))

# - Available Environments


def localhost():
    config = ConfigFile(env.project_settings_file)
    env.docker_compose_file = ['docker-compose.yml']

    env.environment = 'localhost'
    env.app_name = config.project.name
    env.app_root = BASE_DIR
    env.virtualenv = _get_virtualenv_path()
    env.pip = os.path.join(env.virtualenv, 'bin', 'pip')
    env.python = os.path.join(env.virtualenv, 'bin', 'python')


def stage(git_branch=None):
    config = ConfigFile(env.project_settings_file)
    env.docker_containers = []
    env.docker_compose_file = ['docker-compose.yml']

    # apply config settings
    env.environment = 'stage'
    env.app_name = config.project.name
    env.git_origin = config.project.git_origin
    env.git_branch = git_branch or config.stage.git_branch
    env.app_root = config.stage.project_dir or os.path.join('/srv/', config.project.name)

    if not config.stage.server_user or not config.stage.server_address:
        raise TypeError(
            'The values of both server_user and server_address '
            'cannot be empty or null.')

    env.hosts = ['%s@%s' % (config.stage.server_user, config.stage.server_address)]


def prod(git_branch=None):
    config = ConfigFile(env.project_settings_file)
    env.docker_containers = ['web', 'worker']
    env.docker_compose_file = ['docker-compose.prod.yml']

    env.environment = 'prod'
    env.app_name = config.project.name
    env.git_origin = config.project.git_origin
    env.git_branch = git_branch or config.prod.git_branch
    env.app_root = config.prod.project_dir

    if not config.prod.server_user or not config.prod.server_address:
        raise TypeError(
            'The values of both server_user and server_address '
            'cannot be empty or null.')

    env.hosts = ['%s@%s' % (config.prod.server_user, config.prod.server_address)]


# - Available Commands

def provision():
    """
    Run the first setup on application.
    """

    if not hasattr(env, 'app_root'):
        sys.stdout.write('ERROR: unknown environment.')
        os.sys.exit(1)

    # starts provision
    start = time.time()

    if env.environment == 'localhost':
        # starts the localhost setup, install the project
        # requirements, run database migrations and any other
        # things you want to put your local environment up.

        sys.stdout.write('%(now)s\nStarting localhost setup...\n\n\n' % {
            'now': datetime.datetime.now().strftime('%A, %d. %B %Y %I:%M%p')})

        with lcd(env.app_root):

            if not os.path.exists(env.virtualenv):
                raise FileNotFoundError((
                    'Virtual environment on path \'%s\' wasn\'t created yet. '
                    'Please create your virtual environment before starts the provision.'
                    '\nYou can create your virtualenv with command: '
                    '\nvirtualenv -p /path/to/python/bin %s'
                    '\nYou can also create your virtualenvwrapper with command: '
                    '\nmkvirtualenv %s -p /path/to/python/bin -a %s'
                ) % (env.virtualenv, env.virtualenv, env.app_name, env.app_root))

            # install all project requirements
            command = '%s install -r %s'
            local(command % (env.pip, 'requirements.pip'))
            local(command % (env.pip, 'requirements-test.pip'))

            # wait until database is ready
            command = '%s wait_for_postgres.py'
            local(command % env.python)

            # make migrations
            command = '%s manage.py makemigrations'
            local(command % env.python)

            # applies django migrations
            command = '%s manage.py migrate'
            local(command % env.python)

            # executes pylint inspections
            local('pylint --exit-zero --reports=n apps/')

            # executes tests
            local('%s manage.py test apps --settings=core.settings.test --verbosity=3 --nomigrations --noinput' % env.python)

            # clear directory
            command = 'find %s -name \'*.pyc\' -delete'
            local(command % env.app_root)

    final = time.time()
    puts('\nexecution finished in %.2fs' % (final - start))
    puts(" ---------------------------------------")
    puts("                             .d$$b      ")
    puts("                           .' TO$;\     ")
    puts("                          /  : TP._;    ")
    puts("                         / _.;  :Tb|    ")
    puts("                        /   /   ;j$j    ")
    puts("                    _.-'       d$$$$    ")
    puts("                  .' ..       d$$$$;    ")
    puts("                 /  /P'      d$$$$P. |\ ")
    puts("                /   '      .d$$$P' |\^'l")
    puts("              .'           `T$P^'''''  :")
    puts("          ._.'      _.'                ;")
    puts("       `-.-'.-'-' ._.       _.-'    .-' ")
    puts("     `.-' _  ._              .-'        ")
    puts("    -(.g$$$$$$$b.              .'       ")
    puts("      ''^^T$$$P^)            .(:        ")
    puts("        _/  -'  /.'         /:/;        ")
    puts("     ._.'-'`-'  ')/         /;/;        ")
    puts("  `-.-'..--''   ' /         /  ;        ")
    puts(" .-' ..--''        -'          :        ")
    puts(" ..--''--.-'         (\      .-(\       ")
    puts("   ..--''              -\(\/;           ")
    puts("     _.                      :          ")
    puts("                             ;`-        ")
    puts("                            :\          ")
    puts("                            ;           ")
    puts(" ---------------------------------------")


def prepare():
    """
    Prepare a remote host to run the project.
    """
    start = time.time()

    # first of all create a ssh key on server
    # if it does not exist yet.
    command = 'test -e /home/%s/.ssh/id_rsa.pub || (ssh-keygen -q -N ""; cat %s/.ssh/id_rsa.pub; ' \
              'read -p "Press any key to continue... " -n1 -s)'
    run(command % (env.user, env.user))

    # grant app folder permissions
    command = 'sudo chown -R %s:%s /srv/'
    run(command % (env.user, env.user))

    command = 'git clone --single-branch --branch %s %s %s'
    run(command % (env.git_branch, env.git_origin, env.app_root))

    puts('Execution finished in %.2fs' % (time.time() - start))
    puts('Done.')


def deploy():
    start = time.time()
    containers = ' '.join(env.docker_containers)

    # update repository
    command = 'cd %s; git reset --hard && git pull && git checkout -B %s origin/%s '
    run(command % (env.app_root, env.git_branch, env.git_branch))

    # stop containers
    docker_compose('stop')

    # build containers
    docker_compose('build ' + containers)

    # build containers
    docker_compose('up -d ' + containers)

    puts(" ________")
    puts('< execution finished in %.2fs >' % (time.time() - start))
    puts(" ---------------------------------------")
    puts("                             .d$$b      ")
    puts("                           .' TO$;\     ")
    puts("                          /  : TP._;    ")
    puts("                         / _.;  :Tb|    ")
    puts("                        /   /   ;j$j    ")
    puts("                    _.-'       d$$$$    ")
    puts("                  .' ..       d$$$$;    ")
    puts("                 /  /P'      d$$$$P. |\ ")
    puts("                /   '      .d$$$P' |\^'l")
    puts("              .'           `T$P^'''''  :")
    puts("          ._.'      _.'                ;")
    puts("       `-.-'.-'-' ._.       _.-'    .-' ")
    puts("     `.-' _  ._              .-'        ")
    puts("    -(.g$$$$$$$b.              .'       ")
    puts("      ''^^T$$$P^)            .(:        ")
    puts("        _/  -'  /.'         /:/;        ")
    puts("     ._.'-'`-'  ')/         /;/;        ")
    puts("  `-.-'..--''   ' /         /  ;        ")
    puts(" .-' ..--''        -'          :        ")
    puts(" ..--''--.-'         (\      .-(\       ")
    puts("   ..--''              -\(\/;           ")
    puts("     _.                      :          ")
    puts("                             ;`-        ")
    puts("                            :\          ")
    puts("                            ;           ")
    puts(" ---------------------------------------")
