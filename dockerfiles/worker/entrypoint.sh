#!/bin/bash

set -e

# wait until postgres is ready
python wait_for_postgres.py

# create app environments file
printenv | sed 's/^\(.*\)$/export \1/g' | grep -E '^export DATABASE_URL' >> /app_env.sh
printenv | sed 's/^\(.*\)$/export \1/g' | grep -E '^export DJANGO_SETTINGS_MODULE' >> /app_env.sh

# set the environment file permission
chmod +x /app_env.sh

# starts cron job service
cron -f &

# define the fist log when job starts
echo 'CRON has been started successfully!' >> /var/log/cron.log

# track the log file
exec tail -f /var/log/cron.log
