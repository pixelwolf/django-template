#!/bin/bash
set -e

# remove cached sock file
rm -f /srv/web.sock

# restart nginx server
service nginx restart

# wait until postgres is ready
python wait_for_postgres.py

# execute django
python manage.py migrate --noinput
python manage.py collectstatic --noinput
gunicorn --workers=3  --max-requests=1200  --timeout=120  --bind=unix:/srv/web.sock core.wsgi:application

exec "$@"
