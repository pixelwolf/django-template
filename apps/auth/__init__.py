# coding: utf-8

# To grant a custom config to the app is necessary
# apply this variable on module file.
default_app_config = 'apps.auth.apps.AuthConfig'
