from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'apps.auth'

    # to resolve the app name conflict is necessary
    # create another namespace to auth app.
    label = 'user_auth'
