from django.contrib import admin
from django.contrib.auth.models import Group

# Remove default django permissions admin
admin.site.unregister(Group)
