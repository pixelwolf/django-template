# coding: utf-8

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.db.models import Q
from django.utils import timezone
from rest_framework.authtoken.models import Token

from apps.utils.models.base import AbstractBaseModel
from apps.utils.models.base import QuerySet


class UserQueryset(QuerySet):

    def delete_queryset(self, queryset, **kwargs):
        # Exclude all users tokens
        # Uses lookups query to increase the performance.
        lookups = Q()

        for user in queryset:
            lookups |= Q(user=user)

        Token.objects.filter(lookups).delete()

        # Perform the deletion
        super(UserQueryset, self).delete_queryset(
            queryset, status=2, is_active=False,
            **kwargs)


class UserManager(BaseUserManager.from_queryset(UserQueryset)):
    """
    Apply a custom creation from
    custom users on this project.
    """

    def get_queryset(self):
        queryset = super(UserManager, self).get_queryset()
        return queryset.filter(is_deleted=False)

    def create_superuser(self, email, first_name, password, *args, **kwargs):
        """
        Create a superuser on project.
        """

        # create a user on project active
        # and with superuser flag.
        user = self.create(
            email=email,
            first_name=first_name,
            is_superuser=True,
            *args, **kwargs)

        # apply the user password to set
        # user able to login
        user.set_password(password)
        user.save()

        return user


class AbstractUser(AbstractBaseUser, PermissionsMixin, AbstractBaseModel):
    """
    A custom user to grant permissions to login
    and access data from api or other modules
    in this project.
    """

    first_name = models.CharField(
        'Nome', max_length=60)

    last_name = models.CharField(
        'Sobrenome', max_length=60)

    email = models.EmailField(
        'Email', unique=True, null=True)

    date_joined = models.DateTimeField(
        'Data de Cadastro', default=timezone.now)

    is_superuser = models.BooleanField(
        'Super Usuário', default=False)

    is_active = True      # Defines that the user is able to perform actions.

    # Django user settings

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name']

    # Managers

    objects = UserManager()

    class Meta:
        abstract = True

    def __str__(self):
        return self.get_full_name()

    @property
    def is_staff(self):
        """
        If the user is superuser it has totally access to admin site.
        """
        return self.is_superuser

    def get_full_name(self):
        """
        Returns the user complete name using the
        user first and last name.
        """
        full_name = ' '.join([self.first_name, self.last_name or ''])

        return full_name.strip()

    def get_short_name(self):
        return self.first_name
