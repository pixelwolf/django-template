# coding: utf-8
from rest_framework import exceptions
from rest_framework.authentication import TokenAuthentication as BaseTokenAuthentication


class TokenAuthentication(BaseTokenAuthentication):

    def authenticate(self, request):
        """
        Deny the access to administration users on api
        through token auth.
        """
        credentials = super(TokenAuthentication, self).authenticate(request)

        if not credentials:
            raise exceptions.PermissionDenied({
                'code': 'PERMISSION_DENIED',
                'detail': 'Usuários administradores não tem acesso à esses recursos.'
            })
        return credentials
