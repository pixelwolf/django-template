# coding: utf-8
from django.conf import settings
from django.db import models, transaction

from apps.auth.models import AbstractUser
from apps.utils.admin.decorators import admin_property
from apps.utils.models import AbstractBaseModel


class User(AbstractUser):
    STATUS_ONLINE = 1
    STATUS_OFFLINE = 2

    status = models.PositiveSmallIntegerField(
        'Status', choices=(
            (STATUS_ONLINE, 'Online'),
            (STATUS_OFFLINE, 'Offline')
        ), default=STATUS_OFFLINE)

    is_active = models.BooleanField(
        'Ativo', default=True)

    class Meta:
        verbose_name = 'Usuário'
        verbose_name_plural = 'Usuários'
        ordering = ['first_name']

    @transaction.atomic()
    def delete(self, using=None, keep_parents=False):
        """
        Update status and is_active flag on soft delete.
        """
        # Delete active tokens
        self.auth_token.delete()

        # Update attributes
        self.status = User.STATUS_OFFLINE
        self.is_active = False

        super(User, self).delete(using=using, keep_parents=keep_parents)

    @property
    def online(self):
        """
        Returns a boolean if the user is online.
        """
        return self.status == User.STATUS_ONLINE

    @admin_property('Online')
    def get_online(self):
        """
        This is an accessor to admin field display an
        information on object details.
        """
        return self.online
