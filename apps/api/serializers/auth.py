# coding: utf-8
from rest_framework import serializers
from rest_framework.compat import authenticate


class AuthTokenSerializer(serializers.Serializer):
    email = serializers.CharField(label="Email")

    password = serializers.CharField(
        label="Senha",
        style={'input_type': 'password'},
        trim_whitespace=False)

    default_error_messages = {
        'invalid_credentials': (
            'Por favor, insira um Email e Senha corretos. '
            'Note que o campo de senha é sensível letras maiúsculas e minúsculas.'
        ),
    }

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = authenticate(
                request=self.context.get('request'),
                username=email,
                password=password)

            # The authenticate call simply returns None for is_active=False
            # users. (Assuming the default ModelBackend authentication
            # backend.)
            if not user:
                raise serializers.ValidationError({
                    'email': self.default_error_messages['invalid_credentials']
                }, code='authorization')

        else:
            raise serializers.ValidationError({
                'email': self.default_error_messages['invalid_credentials']
            }, code='authorization')

        attrs['user'] = user
        return attrs

    def update(self, instance, validated_data):
        """
        Override to ignore this method.
        """
        pass

    def create(self, validated_data):
        """
        Override to ignore this method.
        """
        pass
