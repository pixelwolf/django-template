# coding: utf-8
from rest_framework import serializers

from apps.api import models


class UserSerializer(serializers.ModelSerializer):

    full_name = serializers.CharField(source='get_full_name')

    class Meta:
        model = models.User
        fields = [
            'first_name',
            'last_name',
            'full_name',
            'email',
            'date_joined'
        ]
