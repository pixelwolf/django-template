# coding: utf-8
from rest_framework.authentication import get_authorization_header
from rest_framework.permissions import BasePermission

from apps.utils.settings import get_settings


class PublicApiPermission(BasePermission):
    """
    For open api, it is necessary to have at least a
    simple way to grant that only authorized uses
    have access.

    To grant this we will check if there is a token on
    http authorization that allows the access
    to endpoint.
    """
    message = None

    def has_permission(self, request, view):
        """
        Check the authorization header to get the generic
        access token and validate it.
        """
        auth = get_authorization_header(request).split()

        # Checks if the access token is authorized as public
        if not auth or not auth[0].lower() == 'public' or len(auth) == 1:
            self.message = 'Cabeçalho de authenticação inválido. Nenhuma credencial fornecida.'
            return False

        if len(auth) > 2:
            self.message = 'Cabeçalho de authenticação inválido. A credencial não pode conter espaços em branco.'

        # Gets the authorized public token to access the api
        authorized_public_token = get_settings('PUBLIC_API_ACCESS_TOKEN')

        if auth[1] != authorized_public_token:
            self.message = 'Credencial Inválida.'
            return False

        return True
