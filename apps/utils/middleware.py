# coding: utf-8

from django.conf import settings
from django.utils.deprecation import MiddlewareMixin


class CrossOriginResourceSharingMiddleware(MiddlewareMixin):
    """
    Cross-origin resource sharing (CORS) is a mechanism that allows restricted
    resources (e.g. fonts) on a web page to be requested from another domain
    outside the domain from which the first resource was served.
    (https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
    """

    allowed_headers = getattr(
        settings, 'CORS_ALLOWED_HEADER', [
            'Authorization',
            'Content-Type',
            'X-Requested-With',
            'User-Agent'
        ])

    allowed_origins = getattr(settings, 'CORS_ALLOWED_ORIGINS', ['*'])

    allowed_methods = getattr(
        settings, 'CORS_ALLOWED_METHODS',
        ['GET', 'POST', 'PUT', 'DELETE', 'PATCH', 'OPTIONS'])

    max_age = getattr(settings, 'CORS_MAX_AGE', None)

    def process_response(self, request, response):
        """
        Define the rules of CORS access on each
        response from project.
        """

        # set response headers
        response['Access-Control-Allow-Headers'] = ','.join(self.allowed_headers)
        response['Access-Control-Allow-Methods'] = ','.join(self.allowed_methods)
        response['Access-Control-Allow-Origin'] = ','.join(self.allowed_origins)

        if self.max_age:
            response['Access-Control-Max-Age'] = self.max_age

        return response
