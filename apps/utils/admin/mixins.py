# coding: utf-8
from functools import update_wrapper
from django.urls import path


class AdminViewMixin:
    extra_views = []

    def get_extra_view(self, view_func_name):
        view = getattr(self, view_func_name, None)

        # get a name for the extra view
        name = getattr(view, 'name')

        # get a route for the extra view
        default_route = '<path:object_id>/%s/' % name
        route = getattr(view, 'route', None) or default_route

        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        return wrap(view), name, route

    def get_extra_urls(self):
        opts = getattr(self.model, '_meta')
        app_label = opts.app_label
        model_name = opts.model_name

        urlpatterns = []

        for view_func_name in self.extra_views:
            view, name, route = self.get_extra_view(view_func_name)
            url = path(route, view, name='%s_%s_%s' % (app_label, model_name, name))
            urlpatterns.append(url)

        return urlpatterns

    def get_urls(self):
        urlpatterns = super(AdminViewMixin, self).get_urls()
        extra_urlpatterns = self.get_extra_urls()
        return extra_urlpatterns + urlpatterns
