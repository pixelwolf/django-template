# coding: utf-8


def admin_list_display(short_description, boolean=False, order_field=None):
    def wrap(func):
        func.short_description = short_description
        func.boolean = boolean
        func.admin_order_field = order_field
        return func
    return wrap


def admin_view(route=None, name=None):
    def wrap(func):
        func.name = name or func.__name__.replace('_view', '')
        func.route = route
        return func
    return wrap
