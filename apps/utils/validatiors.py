# coding: utf-8

from django.core.exceptions import ValidationError
from django.utils.deconstruct import deconstructible
from django.utils.translation import ugettext as _
from jsonschema import ValidationError as SchemaValidationError
from jsonschema import validate as validate_schema


@deconstructible
class JsonSchemaValidator:
    """
    Uses json schema to validate the field content.
    https://pypi.org/project/jsonschema/
    """
    message = _('Invalid JSON Format.')
    code = 'invalid_value'

    def __init__(self, schemas, message=None):
        self.schemas = schemas
        self.message = message or self.message

    def __call__(self, value):
        cleaned = self.clean(value)

        if not any([self.compare(cleaned, schema) for schema in self.schemas]):
            raise ValidationError(self.message, code=self.code)

    def compare(self, value, schema):
        try:
            validate_schema(value, schema)
            return True
        except SchemaValidationError:
            return False

    def __eq__(self, other):
        return (
            isinstance(other, self.__class__) and
            self.schemas == other.schemas and
            self.message == other.message and
            self.code == other.code
        )

    def clean(self, x):
        return x
