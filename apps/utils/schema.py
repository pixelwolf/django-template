# coding: utf-8
from jsonschema import ValidationError as SchemaValidationError
from jsonschema import validate


def is_schema_valid(schema, value):
    """
    Checks the schema and returns if it is valid.
    """
    try:
        validate(value, schema)
        return True
    except SchemaValidationError:
        return False


class TestSchemaMixin:
    """
    Provides the ability to validate schema
    """

    def assertSchema(self, schema, value, msg=None):
        """
        Asserts the schema to validate returned value.
        """

        if not is_schema_valid(schema, value):
            msg = msg or 'Invalid value from provided schema.'
            raise AssertionError(msg)
