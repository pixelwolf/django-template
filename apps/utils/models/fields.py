# coding: utf-8
from django.db import models


class CharNullField(models.CharField):
    description = (
        'CharField that does not accept empty string '
        'and returns None instead.'
    )

    def get_prep_value(self, value):
        """
        The values mustn't be an empty string,
        so forces to return null instead.
        """
        value = super(CharNullField, self).get_prep_value(value)

        if not value:
            return None

        return value
