# coding: utf-8
from datetime import timezone

from django.db import models


class AuditMixin(models.Model):
    created_at = models.DateTimeField(
        "Criado em",
        auto_now_add=True)

    updated_at = models.DateTimeField(
        "Ultima Atualização em",
        auto_now=True)

    class Meta:
        abstract = True


class SoftDeleteMixin(models.Model):
    """
    A simple model mixin to prevent hard deletion
    of objects, instead that just set the object
    as deleted through a deletion flag.
    """
    is_deleted = models.BooleanField(
        'Deletado', default=False)

    deleted_at = models.DateTimeField(
        'Deletado em', null=True, blank=True)

    class Meta:
        abstract = True

    def delete(self, using=None, keep_parents=False):
        """
        Do not permit that the original deletion
        function to be called.

        Instead just change the deletion properties
        and save the object.
        """
        self.is_deleted = True
        self.deleted_at = timezone.now()
        self.save(using=using)
