# coding: utf-8


class ObjectDict(dict):
    """
    Gives a simple dict an object behavior.
    """

    def _get_value_from_key(self, item):
        """
        Get a value from a key into dict.
        """
        try:
            value = self[item]

            # Nested dict must apply the same behavior
            # than parent dict
            if isinstance(value, dict):
                value = ObjectDict(value)

            # Grant the ability to apply the object behavior
            # to dictionaries inside lists.
            elif isinstance(value, (tuple, list)):
                value = [ObjectDict(v) if isinstance(v, dict) else v for v in value]

            return value

        except KeyError:
            raise AttributeError('type object \'{name}\' has no attribute {attr}'.format(
                name=self.__class__.__name__,
                attr=item
            ))

    def __getattr__(self, item):
        """
        Tries to return a object attribute or a object key.
        """
        try:
            return self.__getattribute__(item)
        except AttributeError:
            return self._get_value_from_key(item)
