# coding: utf-8

from django.conf import settings as django_settings
from django.core.exceptions import ImproperlyConfigured


def get_settings(key, default=None, quiet=True):
    """
    Returns a settings from django settings based on a key,
    if the key does not exists and is not allowed quiet
    response raises a improperly configuration error.
    """
    value = getattr(django_settings, key, None)

    if not value and not quiet:
        raise ImproperlyConfigured(
            '%s definition is required on django settings.' % key)

    return value or default
