# coding: utf-8

from dj_database_url import config

import sys
import psycopg2
import time


def is_ready():
    db_settings = config('DATABASE_URL', 'postgres://postgres:@localhost:5432/{{ project_name }}')

    try:
        connection = psycopg2.connect(
            database=db_settings['NAME'],
            user=db_settings['USER'],
            password=db_settings['PASSWORD'],
            host=db_settings['HOST'],
            port=db_settings['PORT']
        )

        connection.close()
    except psycopg2.OperationalError:
        return False

    return True


if __name__ == '__main__':

    loops = 100

    while not is_ready():
        print('Postgres db is not ready yet.')
        time.sleep(1)
        loops -= 1

        if loops == 0:
            print('Postgres db is not accessible.')
            sys.exit(-1)

    sys.exit(0)
