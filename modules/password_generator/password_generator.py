import random


def generate_password(length=6):
    """
    Returns a random password with a defined length.
    """
    available_chars = (
        'abcdefghijklmnopqrstuvwxyz'
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        '0123456789'
    )

    return ''.join(random.choices(available_chars, k=length))
