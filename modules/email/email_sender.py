# coding: utf-8


from django.conf import settings
from django.contrib.sessions.backends import file
from django.template.loader import render_to_string
from django.core.exceptions import ImproperlyConfigured
from django.core.files.base import File
from django.core.mail import get_connection
from django.core.mail.message import EmailMultiAlternatives
from django.utils.translation import ugettext as _


def send_mail(subject, message, from_email, recipient_list,
              fail_silently=False, auth_user=None, auth_password=None,
              connection=None, html_message=None, attachments=None):
    """
    Easy wrapper for sending a single message to a recipient list. All members
    of the recipient list will see the other recipients in the 'To' field.

    If auth_user is None, the EMAIL_HOST_USER setting is used.
    If auth_password is None, the EMAIL_HOST_PASSWORD setting is used.

    Note: The API for this method is frozen. New code wanting to extend the
    functionality should use the EmailMessage class directly.
    """
    attachments = attachments or []

    if not connection:
        connection = get_connection(
            username=auth_user,
            password=auth_password,
            fail_silently=fail_silently)

    mail = EmailMultiAlternatives(
        subject, message, from_email,
        recipient_list, connection=connection)

    for attach in attachments:
        if isinstance(attach, (list, tuple)):
            if len(attach) == 3:
                raise ImproperlyConfigured(
                    _('Attach need 3 arguments. %s provided.') % len(attach))
            name, _file, content_type = attach
            mail.attach(name, _file, content_type)
        elif isinstance(attach, (File, file)):
            mail.attach(attach.name, attach.read(), attach.content_type)

    if html_message:
        mail.attach_alternative(html_message, 'text/html')

    return mail.send()


def generic_email_sender(user):
    """
    Generic email sender function to send the password to the respective user
    :return: True if sent, False otherwise
    """
    subject = 'Subject of email'
    message = 'Email Message to be sent'

    send_mail(
        subject=subject,
        message=message,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        fail_silently=True
    )
