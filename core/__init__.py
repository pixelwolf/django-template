# coding: utf-8
from django.contrib.auth import get_user_model
from django.db.models.signals import post_migrate
from django.dispatch import receiver


@receiver(post_migrate)
def add_super_admin_signal(app_config, *args, **kwargs):
    """
    Add super user on first syncdb.
    """
    user_model = get_user_model()
    superuser_exists = user_model.objects.filter(is_superuser=True).exists()

    if app_config.label == 'auth' and not superuser_exists:

        user_model.objects.create_superuser(
            first_name="Pixelwolf",
            last_name="Webmaster",
            email="webmaster@pixelwolf.com.br",
            password="Pxmatilh4!",
        )
