"""
Django settings for {{ project_name }} project.

Generated by 'django-admin startproject' using Django 2.1.1.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""
import os

from dj_database_url import config


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.getenv('SECRET_KEY', '{{ secret_key }}')


# Application definition

PROJECT_APPS = [
    'apps.api',
    'apps.auth'
]


THIRD_PART_APPS = [

    # Django Rest Framework
    'rest_framework'
]


INSTALLED_APPS = [
    'apps.pxadmin',
    'admin_shortcuts',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
] + THIRD_PART_APPS + PROJECT_APPS


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    # CORS middleware
    'apps.utils.middleware.CrossOriginResourceSharingMiddleware',

    # Rate Limit middleware
    'ratelimit.middleware.RatelimitMiddleware',
]

ROOT_URLCONF = 'core.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'core.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': config('DATABASE_URL', 'postgres://postgres:@localhost:5432/{{ project_name }}')
}


# Admin Customization
# https://docs.djangoproject.com/en/2.1/ref/contrib/admin/#adminsite-attributes

ADMIN_SITE_TITLE = '{{ project_name }}'
ADMIN_SITE_HEADER = '{{ project_name }}'
ADMIN_SITE_INDEX_TITLE = '{{ project_name }}'

# The model to use to represent a User.
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-user-model

AUTH_USER_MODEL = 'api.User'


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Base Upload Folder
# Setup the folder where all files will be uploaded.

UPLOAD_PATH = 'uploads/'


# Django Rest Framework Settings
# http://www.django-rest-framework.org/

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'apps.api.authentication.TokenAuthentication',

    ),
    'PAGE_SIZE': 10
}

# Email Settings
# https://docs.djangoproject.com/en/2.1/ref/settings/#std:setting-EMAIL_HOST

EMAIL_HOST = os.getenv('EMAIL_HOST', 'smtp.gmail.com')
EMAIL_HOST_USER = DEFAULT_FROM_EMAIL = os.getenv('EMAIL_HOST_USER', 'dev@pixelwolf.com.br')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')
EMAIL_PORT = os.getenv('EMAIL_PORT', 587)
EMAIL_USE_TLS = True

# Public Api Access Token
# We are validating an access token to grant access to public apis.
# That is important for not permit an unauthorized access.

PUBLIC_API_ACCESS_TOKEN = os.getenv('PUBLIC_API_ACCESS_TOKEN', '88703985df47441a90709234ff70e7ab')

# Admin Shortcuts
# Docs inl: https://github.com/alesdotio/django-admin-shortcuts
ADMIN_SHORTCUTS = [
    {
        'shortcuts': [
            {
                'url': '/pxadmin/',
                'open_new_window': False,
            },
        ]
    },
]

ADMIN_SHORTCUTS_SETTINGS = {
    'show_on_all_pages': True,
    'hide_app_list': False,
    'open_new_window': False,
}

# Rate limit settings
# docs in: https://django-ratelimit.readthedocs.io/en/v1.0.0/settings.html
RATELIMIT_USE_CACHE = 'default'
RATELIMIT_ENABLE = True
RATELIMIT_VIEW = 'apps.api.views.ratelimited.been_limited'
RATELIMIT = os.getenv('RATE_LIMIT', '10/s')
